// Karma configuration for unit tests

var baseConfig = require('./karma-base.conf.js');

module.exports = function(config){
    // Load base config
    baseConfig(config, {

        name: "Unit",

        report: "unit",

        files: [
            // IE support for sinon
            'node_modules/sinon/lib/sinon/util/xdr_ie.js',
            'node_modules/sinon/lib/sinon/util/xhr_ie.js',

            // Our ES6 src modules (transpiled to JS)
            'src/main/frontend/web-resource/**/*.es6',

            // Our ES6 test modules (transpiled to JS)
            'src/test/frontend/util/*.es6',
            'src/test/frontend/unit/*.es6'
        ],

        coverage: true
    });
};
