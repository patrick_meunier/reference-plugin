/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package ut;

import com.adaptavist.reference.service.HitCounter;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HitCounterTest {

    @Test
    public void hitCounterEquals() throws Exception {
        assertThat(new HitCounter(2), is(equalTo(new HitCounter(2))));
    }

    @Test
    public void hitCounterSameInstance() throws Exception {
        final HitCounter hitCounter = new HitCounter(2);
        assertThat(hitCounter, is(equalTo(hitCounter)));
    }

    @Test
    public void hitCounterNotEquals() throws Exception {
        assertThat(new HitCounter(1), is(not(equalTo(new HitCounter(10)))));
    }

    @Test
    public void hitCounterNotEqualsToNull() throws Exception {
        assertThat(new HitCounter(1), is(not(equalTo(null))));
    }

    @Test
    public void hitCounterNotEqualsToNonHitCounter() throws Exception {
        assertThat(new HitCounter(1), is(not(equalTo(1L))));
    }

    @Test
    public void hitCounterHashCodeEqual() throws Exception {
        assertThat(new HitCounter(2).hashCode(), is(equalTo(new HitCounter(2).hashCode())));
    }

    @Test
    public void hitCounterToStringEqual() throws Exception {
        assertThat(new HitCounter(2).toString(), is(equalTo(new HitCounter(2).toString())));
    }

    @Test
    public void jsonRepresention() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();

        final HitCounter original = new HitCounter(100);
        final HitCounter roundTrip = objectMapper.readValue(objectMapper.writeValueAsString(original), HitCounter.class);

        assertThat(roundTrip, is(not(sameInstance(original))));
        assertThat(roundTrip, is(equalTo(original)));
    }
}
