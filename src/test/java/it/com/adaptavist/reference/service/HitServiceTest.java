/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.reference.service;

import com.adaptavist.reference.service.HitCounter;
import com.adaptavist.reference.service.HitService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

// Here we'll test a service component exposed by our plugin directly inside the application using Arquillian.

@RunWith(Arquillian.class)
public class HitServiceTest {

    // Components can be injected into our test using @ComponentImport
    @ComponentImport
    @Inject
    public HitService hitService;

    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment
    // to an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //
    // This particular method will wrap this test class into a plugin.
    //
    // NOTE: Since arquillian 1.3.0 & remote-atlas-container 3.3.0, it is no longer necessary to include a
    // @Deployment for simple cases such as this, so it's commented out now, but left for historical sake.
    // You can uncomment it and use it still if you'd prefer it to be explicit.
    //
//    @Deployment
//    public static Archive<?> deployTests() {
//        return ShrinkWrap.create(JavaArchive.class, "tests.jar");
//    }

    @Test
    public void hitItIncreasesCounterByOne() throws Exception {
        given_hit_counter_is_zero();

        when_hit();

        then_hit_counter_is(1);

        when_hit();

        then_hit_counter_is(2);
    }

    @Test
    public void hitCounterCanBeReset() throws Exception {
        given_hit_counter_is_not_zero();

        when_reset();

        then_hit_counter_is(0);
    }

    private void given_hit_counter_is_zero() {
        hitService.reset();
    }

    private void given_hit_counter_is_not_zero() {
        hitService.hitIt();
        assertThat(hitService.getCount().count, is(greaterThan(0L)));
    }

    private void when_hit() {
        hitService.hitIt();
    }

    private void when_reset() {
        hitService.reset();
    }

    private void then_hit_counter_is(long expectedCount) {
        final HitCounter counter = hitService.getCount();

        assertThat("Count was not as expected", counter.count, is(equalTo(expectedCount)));
    }
}
