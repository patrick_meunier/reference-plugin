/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.reference.rest;

import com.adaptavist.reference.service.HitCounter;
import com.adaptavist.reference.service.HitService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.squareup.okhttp.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.arquillian.container.test.api.BeforeDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

// We'll test the REST API of out plugin using Arquillian, which allows us to run tests both as client and inside the
// application. This allows us to setup the internal state of the plugin/application so we can test the REST API
// returns the response we expect, and also check the internal state after a request.
//
// Test methods will be run inside the application by default, by annotating with @RunAsClient they will run
// outside of the application. The @InSequence annotation enforces execution order of tests, allowing alternating
// of in-app and as-client tests.

@RunWith(Arquillian.class)
public class HitsResourceTest {

    private final String REST_ENDPOINT = "rest/adaptavist-reference/latest/hits";

    private final OkHttpClient okHttpClient = new OkHttpClient();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @ArquillianResource
    public URL baseUrl;

    @ComponentImport
    @Inject
    public HitService hitService;

    private String username;
    private Response response;

    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //
    // This particular method will wrap this test class and the additional packages given into a plugin.
    //
    // NOTE: Since arquillian 1.3.0 & remote-atlas-container 3.3.0, it is no longer necessary to include a
    // @Deployment for simple cases. But if you need to customise the deployment, adding packages as we do
    // here, then you can either include this still, or use a @BeforeDeployment static method - as shown below.
    //
//    @Deployment
//    public static Archive<?> deployTests() {
//        return ShrinkWrap.create(JavaArchive.class, "tests.jar")
//                .addPackages(true, "com.squareup.okhttp");
//    }

    // This allows us to benefit from the automatic deployment generation, and still customise it before deployment.
    @BeforeDeployment
    public static JavaArchive addDeploymentContent(JavaArchive archive) {
        return archive.addPackages(true, "com.squareup.okhttp");
    }

    // Test anonymous access

    @Test
    @RunAsClient
    @InSequence(11)
    public void anonymousGetIsUnauthorized() throws Exception {
        given_anon_user();

        when_get_hits();

        then_response_is_unauthorized();
    }

    @Test
    @RunAsClient
    @InSequence(12)
    public void anonymousPostIsUnauthorized() throws Exception {
        given_anon_user();

        when_post_hits();

        then_response_is_unauthorized();
    }

    @Test
    @RunAsClient
    @InSequence(13)
    public void anonymousDeleteIsUnauthorized() throws Exception {
        given_anon_user();

        when_delete_hits();

        then_response_is_unauthorized();
    }

    // Test authorized use-case

    @Test
    @InSequence(101)
    public void givenHitCounterStartsAtZero() throws Exception {
        hitService.reset();
    }

    @Test
    @RunAsClient
    @InSequence(102)
    public void initialGetReturnsZero() throws Exception {
        given_valid_user();

        when_get_hits();

        then_response_is_successful();
        then_response_contains_count_of(0);
    }

    @Test
    @RunAsClient
    @InSequence(103)
    public void postIncrementsCounter() throws Exception {
        given_valid_user();

        when_post_hits();

        then_response_is_successful();
    }

    @Test
    @InSequence(104)
    public void internalCounterHasBeenIncremented() throws Exception {
        then_internal_hit_counter_is(1);
    }

    @Test
    @RunAsClient
    @InSequence(105)
    public void getReturnsIncrementedCount() throws Exception {
        given_valid_user();

        when_get_hits();

        then_response_is_successful();
        then_response_contains_count_of(1);
    }

    @Test
    @RunAsClient
    @InSequence(106)
    public void deleteResetsCounter() throws Exception {
        given_valid_user();

        when_delete_hits();

        then_response_is_successful();
    }

    @Test
    @InSequence(107)
    public void internalCounterHasBeenReset() throws Exception {
        then_internal_hit_counter_is(0);
    }

    // Givens

    private void given_anon_user() {
        username = null;
    }

    private void given_valid_user() {
        username = "admin";
    }

    // Whens

    private void when_get_hits() throws IOException {
        response = okHttpClient.newCall(request().get().build()).execute();
    }

    private void when_post_hits() throws IOException {
        response = okHttpClient.newCall(request().post(RequestBody.create(MediaType.parse("text/plain"), "")).build()).execute();
    }

    private void when_delete_hits() throws IOException {
        response = okHttpClient.newCall(request().delete().build()).execute();
    }

    // Thens

    private void then_response_is_successful() {
        assertThat("Expected successful response status", response.code(), isOneOf(200, 204));
    }

    private void then_response_is_unauthorized() {
        assertThat("Expected unauthorized response status", response.code(), is(401));
    }

    private void then_response_contains_count_of(long count) throws IOException {
        final HitCounter expectedValue = new HitCounter(count);
        final HitCounter responseValue = objectMapper.readValue(response.body().charStream(), HitCounter.class);

        assertThat(responseValue, is(equalTo(expectedValue)));
    }

    private void then_internal_hit_counter_is(long count) {
        assertThat(hitService.getCount().count, is(equalTo(count)));
    }

    // Utils

    private Request.Builder request() {
        final Request.Builder builder = new Request.Builder()
                .url(baseUrl + REST_ENDPOINT)
                .addHeader("Accept", "application/json");

        if (username != null) {
            builder.addHeader("Authorization", Credentials.basic(username, username));
        }

        return builder;
    }
}
