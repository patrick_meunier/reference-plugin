/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.reference.ui;

import com.adaptavist.arquillian.FailFast;
import it.com.adaptavist.reference.ui.pages.HitReportPageWithDebugging;
import it.com.adaptavist.reference.ui.pages.LoginPage;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.graphene.page.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(Arquillian.class)
@RunAsClient
@FailFast
public class RedirectViaLoginPreservesUrlParamsTest extends UiTestBase {

    @Test
    @InSequence(10)
    public void given_we_are_not_logged_in() {
        // Do nothing, as a new session is started for each test class
    }

    @Test
    @InSequence(20)
    public void and_we_attempt_to_view_hit_count(@InitialPage HitReportPageWithDebugging hitReportPage) {
        // Do nothing, as the @InitialPage will take us to the desired page
    }

    @Test
    @InSequence(30)
    public void when_we_login(@Page LoginPage loginPage) {
        loginPage.assertOnLoginPage();
        loginPage.login("admin", "admin");

    }

    @Test
    @InSequence(40)
    public void then_we_are_redirected_back_to_the_original_page(@Page HitReportPageWithDebugging hitReportPage) {
        assertTrue("Expected to see debug param in URL", hitReportPage.isDebugEnabled());
    }

}
