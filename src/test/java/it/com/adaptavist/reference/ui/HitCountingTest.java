/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.reference.ui;

// We'll test the actual UI of our plugin using Arquillian, which allows us to run tests both as client and inside the
// application. This allows us to setup the internal state of the plugin/application so we can test the UI behaves as
// we expect, and also check the internal state after interacting with the UI.
//
// Test methods will be run inside the application by default, by annotating with @RunAsClient they will run
// outside of the application. The @InSequence annotation enforces execution order of tests, allowing alternating
// of in-app and as-client tests.

import com.adaptavist.arquillian.FailFast;
import it.com.adaptavist.reference.ui.pages.LoginPage;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(Arquillian.class)
@FailFast
public class HitCountingTest extends UiTestBase {

    @Test
    @InSequence(10)
    public void given_hit_counter_is_zero() throws Exception {
        hitService.reset();
        assertThat("Expected hit counter to have been reset", hitService.getCount().count, is(equalTo(0L)));
    }

    @Test
    @RunAsClient
    @InSequence(20)
    public void when_we_login_and_redirect_to_dashboard(@InitialPage LoginPage loginPage) throws Exception {
        loginPage.assertOnLoginPage();

        loginPage.login("admin", "admin");

        loginPage.assertOnDashboard();
    }

    @Test
    @InSequence(30)
    public void then_hit_counter_is_one() throws Exception {
        assertThat("Expected one hit", hitService.getCount().count, is(equalTo(1L)));
    }
}
