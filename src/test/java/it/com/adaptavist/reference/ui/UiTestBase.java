/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.reference.ui;

import com.adaptavist.reference.service.HitService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import it.com.adaptavist.reference.ui.pages.LoginPage;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

/**
 * Base test class for all of our UI tests.
 * <p>
 * This will hold common injected components, deployment methods, and utility fns.
 */
public abstract class UiTestBase {

    @Drone
    protected WebDriver browser;

    @ComponentImport
    @Inject
    protected HitService hitService;

    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //
    // This particular method will wrap this test class and the additional packages into a plugin.
    @Deployment
    public static Archive<?> deployTests() {
        return ShrinkWrap.create(JavaArchive.class, "tests.jar")
                .addClass(UiTestBase.class)
                .addPackage(LoginPage.class.getPackage())
                .addClasses(WebDriver.class, SearchContext.class);
    }
}
