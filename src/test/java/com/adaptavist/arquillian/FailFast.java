/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicate that a test class should 'Fail Fast', ie. when a test method fails all further methods will be skipped.
 * Mainly of use when test methods are ordered with @InSequence.
 * <p>
 * A SkipTests value may be given to indicate the scope of further skippage. SkipTests.IN_CLASS is default, and will
 * cause skipping of further tests in the current test class only. SkipTests.IN_SUITE will cause all remaining tests
 * in the entire suite to be skipped.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FailFast {

    SkipTests value() default SkipTests.IN_CLASS;

}
