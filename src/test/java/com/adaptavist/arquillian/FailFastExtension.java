/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian;

import org.jboss.arquillian.core.api.annotation.Observes;
import org.jboss.arquillian.core.spi.LoadableExtension;
import org.jboss.arquillian.test.spi.TestResult;
import org.jboss.arquillian.test.spi.event.suite.AfterSuite;
import org.jboss.arquillian.test.spi.event.suite.AfterTestLifecycleEvent;
import org.jboss.arquillian.test.spi.event.suite.BeforeClass;
import org.jboss.arquillian.test.spi.event.suite.BeforeSuite;
import org.jboss.arquillian.test.spi.execution.ExecutionDecision;
import org.jboss.arquillian.test.spi.execution.TestExecutionDecider;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.util.Optional;

/**
 * Arquillian extension to skip further test methods when one fails in a test class that is annotated with @FailFast
 */
public class FailFastExtension implements LoadableExtension, TestExecutionDecider {

    @Nonnull
    private static SkipTests skipScope = SkipTests.NONE;

    @Nonnull
    private static Optional<Method> failedMethod = Optional.empty();

    @Override
    public void register(ExtensionBuilder builder) {
        builder.observer(FailFastExtension.class);
        builder.service(TestExecutionDecider.class, FailFastExtension.class);
    }

    private void reset() {
        skipScope = SkipTests.NONE;
        failedMethod = Optional.empty();
    }

    public void beforeTestSuite(@Observes BeforeSuite event) {
        reset();
    }

    public void beforeTestClass(@Observes BeforeClass event) {
        if (failedMethod.isPresent() && skipScope == SkipTests.IN_SUITE) {
            // Suite has already failed fast, so don't bother checking for FailFast annotation or resetting failed state
            return;
        }

        reset();

        if (event.getTestClass().isAnnotationPresent(FailFast.class)) {
            skipScope = event.getTestClass().getAnnotation(FailFast.class).value();
        }
    }

    public void gatherTestResult(@Observes AfterTestLifecycleEvent event, TestResult result) {
        if (result.getStatus() == TestResult.Status.FAILED) {
            failedMethod = Optional.of(event.getTestMethod());
        }
    }

    public void afterTestSuite(@Observes AfterSuite event) {
        reset();
    }

    @Override
    public ExecutionDecision decide(Method testMethod) {
        if (failedMethod.isPresent()) {
            if (skipScope == SkipTests.IN_CLASS) {
                return ExecutionDecision.dontExecute("Test failed earlier in class: " + failedMethod.get().getName());
            } else if (skipScope == SkipTests.IN_SUITE) {
                return ExecutionDecision.dontExecute("Test failed earlier in suite: " + formatFullMethodName(failedMethod.get()));
            }
        }

        return ExecutionDecision.execute();
    }

    private String formatFullMethodName(Method method) {
        return method.getDeclaringClass().getName() + "." + method.getName();
    }

    @Override
    public int precedence() {
        return 0;
    }
}
