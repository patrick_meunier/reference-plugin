/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian;

import com.saucelabs.saucerest.SauceREST;
import org.arquillian.drone.saucelabs.extension.webdriver.SauceLabsDriver;
import org.jboss.arquillian.core.api.annotation.Observes;
import org.jboss.arquillian.core.spi.LoadableExtension;
import org.jboss.arquillian.drone.spi.DroneContext;
import org.jboss.arquillian.drone.spi.event.AfterDronePrepared;
import org.jboss.arquillian.drone.spi.event.BeforeDroneDestroyed;
import org.jboss.arquillian.drone.webdriver.configuration.WebDriverConfiguration;
import org.jboss.arquillian.test.spi.TestClass;
import org.jboss.arquillian.test.spi.TestResult;
import org.jboss.arquillian.test.spi.event.suite.AfterTestLifecycleEvent;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Objects;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class SauceLabsJobReportExtension implements LoadableExtension {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private Boolean sessionResult = null;

    @Override
    public void register(ExtensionBuilder builder) {
        builder.observer(SauceLabsJobReportExtension.class);
    }

    // Dynamically set the 'name' capability for the WebDriver instance to the test class name - this is used by SauceLabs to identify the test
    public void setNameCapability(@Observes AfterDronePrepared event, DroneContext context, TestClass testClass) {

        final WebDriverConfiguration configuration = context.get(event.getDronePoint()).getConfigurationAs(WebDriverConfiguration.class);

        // This is a bit of a hack, getting the private capabilityMap out of WebDriverConfiguration and adding additional capabilities
        // but I couldn't see any other way to do this - suggestions/improvements would be most welcome.

        final Map<String, Object> capabilityMap = getCapabilityMap(configuration);

        capabilityMap.put("name", testClass.getName());

        sessionResult = null;
    }

    public void gatherTestResults(@Observes AfterTestLifecycleEvent event, TestResult result) {
        if (sessionResult != FALSE) {
            switch (result.getStatus()) {
                case PASSED:
                    sessionResult = TRUE;
                    break;
                case FAILED:
                    sessionResult = FALSE;
                    break;
                default:
                    // Do nothing
            }
        }
    }

    public void reportResults(@Observes BeforeDroneDestroyed event, DroneContext context, TestClass testClass) {
        if (sessionResult != null) {
            log.info("{}: {}", testClass.getName(), sessionResult ? "PASSED" : "FAILED");

            final WebDriver webDriver = context.get(event.getDronePoint()).getInstanceAs(WebDriver.class);

            if (webDriver instanceof SauceLabsDriver) {
                final String jobId = ((SauceLabsDriver) webDriver).getSessionId().toString();

                final Capabilities capabilities = context.get(event.getDronePoint()).getConfigurationAs(WebDriverConfiguration.class).getCapabilities();

                final String username = Objects.toString(capabilities.getCapability("username"));
                final String accesskey = Objects.toString(capabilities.getCapability("access.key"));

                final SauceREST rest = new SauceREST(username, accesskey);

                if (sessionResult) {
                    rest.jobPassed(jobId);
                } else {
                    rest.jobFailed(jobId);
                }

                log.info(rest.getPublicJobLink(jobId));
            }
        }
    }

    private Map<String, Object> getCapabilityMap(WebDriverConfiguration configuration) {
        try {
            final Field capabilityMap = configuration.getClass().getDeclaredField("capabilityMap");
            if (!capabilityMap.isAccessible()) {
                capabilityMap.setAccessible(true);
            }
            return (Map<String, Object>) capabilityMap.get(configuration);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Could not get capabilityMap from WebDriverConfiguration (" + configuration.getConfigurationName() + ")", e);
        }
    }
}
