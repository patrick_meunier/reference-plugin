/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

jQuery(function($) {
    var $report = $(".adaptavist-hit-report");
    
    if ($report.length) {
        console.log("Loading hit counter...");

        // Fetch the current hit count
        $.getJSON(AJS.contextPath() + "/rest/adaptavist-reference/latest/hits")
            .done(function(data) {
                console.log("Loaded hit counter:", data);

                renderReport(data);
            })
            .fail(function(xhr, message, error) {
                console.error("Failed to load hit counter:", error);

                renderError({ message: message });
            });
    }
    
    function renderReport(data) {
        $report.html(Adaptavist.Reference.Templates.report(data));
    }

    function renderError(data) {
        $report.html(Adaptavist.Reference.Templates.error(data));
    }
});
