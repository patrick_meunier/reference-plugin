/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

jQuery(function($) {
    // Don't count the page if it contains a hit exclude class
    if (!$(".adaptavist-hit-exclude:first").length) {
        console.log("Increment hit counter");

        // Increment the hit counter
        $.post(AJS.contextPath() + "/rest/adaptavist-reference/latest/hits")
            .done(function(data) {
                console.log("Hit counter incremented:", data);
            })
            .fail(function(xhr, status, error) {
                console.error("Failed to increment hit counter:", error);
            });
    }
});
