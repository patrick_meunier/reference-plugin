/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.reference.rest;

import com.adaptavist.reference.service.HitCounter;
import com.adaptavist.reference.service.HitService;
import com.atlassian.annotations.security.XsrfProtectionExcluded;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("hits")
public class HitsResource {

    private final HitService hitService;

    @Inject
    public HitsResource(HitService hitService) {
        this.hitService = hitService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHitCount() {
        final HitCounter count = hitService.getCount();

        return Response.ok(count).build();
    }

    @POST
    @XsrfProtectionExcluded
    public Response hitMe() {
        hitService.hitIt();

        return Response.noContent().build();
    }

    @DELETE
    public Response reset() {
        hitService.reset();

        return Response.noContent().build();
    }
}
