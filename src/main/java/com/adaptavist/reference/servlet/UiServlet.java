/*-
 * #%L
 * Reference Plugin
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.reference.servlet;


import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

public class UiServlet extends HttpServlet implements InitializingBean {

    // Constants
    private static final String TEMPLATE_WEB_RESOURCE = "hits-ui";
    private static final String TEMPLATE_NAME = "Adaptavist.Reference.Templates.document";


    // We use slf4j for logging, this should NOT be static
    private final Logger log = LoggerFactory.getLogger(getClass());

    // Components injected by the constructor
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final BundleContext bundleContext;

    // Other properties
    private String templateModuleKey;

    // Constructor used for injecting components required by this class,
    // @ComponentImport is required for components from outside the plugin, eg. from the application or other plugins.
    // BundleContext is a special case though and provided by the OSGi framework.
    @Inject
    public UiServlet(@ComponentImport UserManager userManager,
                     @ComponentImport LoginUriProvider loginUriProvider,
                     @ComponentImport SoyTemplateRenderer soyTemplateRenderer,
                     BundleContext bundleContext) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.bundleContext = bundleContext;
    }

    // This is executed once this component has been initialised and injection completed
    @Override
    public void afterPropertiesSet() throws Exception {
        // Get out plugin key from the BundleContext so we don't have to hardcode it
        templateModuleKey = OsgiHeaderUtil.getPluginKey(bundleContext.getBundle()) + ":" + TEMPLATE_WEB_RESOURCE;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final UserKey userKey = userManager.getRemoteUserKey(request);
        if (userKey == null) {
            redirectToLogin(request, response);
        } else {
            renderPage(request, response);
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("Redirecting to login page");

        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private void renderPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("Rendering our page");

        response.setContentType("text/html;charset=utf-8");
        soyTemplateRenderer.render(response.getWriter(), templateModuleKey, TEMPLATE_NAME, getTemplateData(request));
    }

    private Map<String, Object> getTemplateData(HttpServletRequest request) {
        final UserProfile userProfile = userManager.getRemoteUser();

        return ImmutableMap.<String, Object>builder()
                .put("debug", "true".equals(request.getParameter("debug")))
                .put("isAdmin", userManager.isSystemAdmin(userProfile.getUserKey()))
                .build();
    }

    // Suppress warning to use StringBuilder instead of StringBuffer
    @SuppressWarnings("squid:S1149")
    private URI getUri(HttpServletRequest request) {
        final StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
