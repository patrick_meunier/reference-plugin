var browsers = {
    sl_chrome: {
        base: 'SauceLabs',
        browserName: 'chrome',
        platform: 'Windows 8.1',
        version: '41'
    },
    sl_firefox: {
        base: 'SauceLabs',
        browserName: 'firefox',
        platform: 'Windows 8.1',
        version: '33'
    },
    sl_ie_11: {
        base: 'SauceLabs',
        browserName: 'internet explorer',
        platform: 'Windows 8.1',
        version: '11'
    },
    sl_ie_10: {
        base: 'SauceLabs',
        browserName: 'internet explorer',
        platform: 'Windows 8',
        version: '10'
    },
    sl_ie_9: {
        base: 'SauceLabs',
        browserName: 'internet explorer',
        platform: 'Windows 7',
        version: '9'
    },
    sl_safari: {
        base: 'SauceLabs',
        browserName: 'safari',
        platform: 'OS X 10.9',
        version: '7'
    }
};

// Disable some sauceLabs browsers for now - we may want to make this configurable by a external var
//delete browsers.sl_chrome;
//delete browsers.sl_firefox;
//delete browsers.sl_ie_9;
//delete browsers.sl_ie_10;
//delete browsers.sl_ie_11;
//delete browsers.sl_safari;

exports.browsers = browsers;

exports.enabled = function() {
    return !!(process.env.SAUCE_USERNAME && process.env.SAUCE_ACCESS_KEY);
};
