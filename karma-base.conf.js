// Karma configuration

var projectName = require("./package.json").name;

var _ = require("lodash");
var saucelabs = require("./saucelabs.conf.js");
var browsers = require("./browsers.conf.js");

module.exports = function (config, options) {

    // Browsers to run on Sauce Labs
    // Check out https://saucelabs.com/platforms for all browser/platform combos
    var sauceLabsLaunchers = _.mapValues(saucelabs.browsers, function (browser) {
        return _.assign({base: 'SauceLabs'}, browser)
    });

    // Browsers to run on local machine (when not using Sauce Labs)
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    var localBrowsers = browsers.localBrowsers;

    var reporters = [
        'progress', 'junit'
    ];

    if (options.coverage) {
        reporters.push('coverage');
    }

    if (saucelabs.enabled()) {
        reporters.push('saucelabs');
    }

    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'chai-sinon', 'jquery-1.7.2'],


        // list of files / patterns to load in the browser
        files: [
            // The module loader used by JIRA and Confluence
            'node_modules/almond/almond.js',

            // Prevent UMD style libs from registering modules without an id
            'src/test/frontend/prevent-umd-define.js',

            // Common libraries available in the application environment

            // underscore library
            'node_modules/underscore/underscore.js',

            // Fix up modules to match what we'd expect in the live application
            'src/test/frontend/fix-modules.js'
        ]
            .concat(options.files)
            .concat([
                // Execute our test modules
                'src/test/frontend/run-tests.js'
            ]),

        // list of files to exclude
        exclude: [],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            '**/*.js': ['sourcemap'],
            'src/main/frontend/**/*.es6': ['babel', 'sourcemap', 'coverage'],
            'src/test/frontend/**/*.es6': ['babel', 'sourcemap']
        },

        babelPreprocessor: {
            options: {
                sourceMaps: 'both',
                retainLines: true,
                moduleIds: true,
                modules: 'amd',
                getModuleId: function (name) {
                    return name.replace(/^.*src\/(main\/frontend\/web-resource|test\/frontend)\/(.+)$/, '$2')
                },
                resolveModuleSource: function (source) {
                    return source.replace(/^\//, ""); // Strip the leading / from module names
                }
            }
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: reporters,

        junitReporter: {
            outputFile: 'target/karma-reports/TEST.' + options.report + '.karma-results.xml'
        },

        coverageReporter: {
            type: 'lcov',
            dir: 'target',
            subdir: 'lcov-' + options.report,

            instrumenter: {
                'src/main/frontend/**/*.es6': 'istanbul'
            }
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        browsers: saucelabs.enabled() ? Object.keys(sauceLabsLaunchers) : localBrowsers,

        // Sauce Labs config
        sauceLabs: {
            testName: projectName + ' ' + options.name + ' (karma)'
        },
        customLaunchers: sauceLabsLaunchers,

        browserDisconnectTimeout: saucelabs.enabled() ? 10000 : 5000,
        browserDisconnectTolerance: saucelabs.enabled() ? 3 : 1,
        browserNoActivityTimeout: saucelabs.enabled() ? 60000 : 30000,

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: saucelabs.enabled() || !!process.env.BUILD_TAG
    });
};
