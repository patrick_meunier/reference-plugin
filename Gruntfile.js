module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        watch: {
            modules: {
                files: [
                    'src/main/frontend/**/*.es6'
                ],
                tasks: ['build']
            }
        },

        // Convert ES6 to ES5
        babel: {
            options: {
                sourceMap: false,
                moduleIds: true,
                modules: 'amd',
                getModuleId: function (name) {
                    return name.replace(/^.*src\/main\/frontend\/web-resource\/(.+)$/, '$1')
                },
                resolveModuleSource: function(source) {
                    return source.replace(/^\//, ""); // Strip the leading / from module names
                }
            },

            main: {
                options: {
                    sourceRoot: 'src/main/frontend/web-resource'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src/main/frontend',
                        src: ['**/*.es6'],
                        dest: 'target/generated-resources',
                        ext: '.js',
                        extDot: 'first'
                    }
                ]
            }
        },

        uglify: {
            options: {
                mangle: false,
                compress: true,
                sourceMap: true,
                sourceMapIn: function(src){
                    return src + '.map';
                }
            },
            modules: {
                files: [{
                    expand: true,
                    cwd: 'target/generated-resources/web-resource',
                    src: ['**/*.js'],
                    dest: 'target/generated-resources/web-resource'
                }]
            }
        }
    });

    grunt.registerTask('clean', function() {
        grunt.file.delete("target/generated-resources");
        grunt.file.delete("target/generated-test-resources");
    });

    // Fix sources property in source map files so they will be relative to the web-resource
    grunt.registerTask('fix-source-maps', function() {
        grunt.file.expand("target/generated-resources/**/*.map").forEach(function(file) {
            var map = grunt.file.readJSON(file);
            var basenames = map.sources.map(function (path) {
                return path.split('/').pop();
            });
            map.sources = basenames.reduce(function(accumulator, value){
                return accumulator.indexOf(value) === -1 ? accumulator.concat(value) : accumulator;
            }, []);
            grunt.file.write(file, JSON.stringify(map));
        });
    });

    grunt.registerTask('build', [
        'babel',
//        'uglify',
        'fix-source-maps'
    ]);

    grunt.registerTask('dev', [
        'build',
        'watch'
    ]);
};
