var localBrowsers = [
    "Firefox",
    "Chrome"
];

// Add IE if running in Windows
if (process.env.Path && /C:\\Program Files/.test(process.env.Path)) {
    localBrowsers.push('IE');
}

exports.localBrowsers = localBrowsers;
