#!/bin/sh
#
# Build the docker image for use in bitbucket-pipeline builds
#

atlas-mvn clean
docker build -t adaptavist/reference-plugin .
