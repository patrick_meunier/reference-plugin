<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <packaging>atlassian-plugin</packaging>

    <parent>
        <groupId>com.adaptavist.pom</groupId>
        <artifactId>adaptavist-base-plugin-pom</artifactId>
        <version>13</version>
    </parent>

    <groupId>com.adaptavist</groupId>
    <artifactId>reference-plugin</artifactId>
    <version>0.0.1-SNAPSHOT</version>

    <name>Reference Plugin</name>
    <description>An example plugin for reference/training/experimenting</description>

    <organization>
        <name>Adaptavist.com Ltd</name>
        <url>http://www.adaptavist.com/</url>
    </organization>

    <!-- If you provide any substantial contribution to a project, please add yourself to the developers list -->
    <developers>
        <developer>
            <name>Mark Gibson</name>
            <email>mgibson@adaptavist.com</email>
        </developer>
    </developers>

    <properties>
        <!-- Set the versions of the applications you wish to build and test against -->
        <confluence.version>5.10.2</confluence.version>
        <confluence.test.version>${confluence.version}</confluence.test.version>

        <jira.version>7.6.0</jira.version>
        <jira.test.version>${jira.version}</jira.test.version>

        <!-- Atlassian components
             versions should target the lowest common denominator, which can be found by checking in the developer
             tools and OSGi (tab in UPM) of each app -->
        <guava.version>18.0</guava.version>
        <spring.version>4.1.6.RELEASE</spring.version>
        <atlassian.plugins.version>4.1.0</atlassian.plugins.version>
        <atlassian.sal.version>3.0.0</atlassian.sal.version>
        <atlassian.rest.version>3.0.0</atlassian.rest.version>
        <atlassian.soy.version>4.3.0</atlassian.soy.version>
        <atlassian.spring.scanner.version>2.1.3</atlassian.spring.scanner.version>

        <!-- Test dependency versions -->
        <selenium.version>2.53.1</selenium.version>
        <arquillian.drone.version>2.0.0.Final</arquillian.drone.version>
        <arquillian.graphene.version>2.1.0.CR2</arquillian.graphene.version>
        <arquillian.junit.version>1.4.0.Final</arquillian.junit.version>
        <arquillian.atlas.version>3.4.0</arquillian.atlas.version>
        <saucelabs.connect.version>2.1.21</saucelabs.connect.version>
        <saucelabs.rest.version>1.0.33</saucelabs.rest.version>

        <!-- Default application for testing -->
        <instanceId>confluence</instanceId>

        <!-- Default browser for testing -->
        <browser>firefox</browser>

        <webdriver.chrome.driver>/usr/local/bin/chromedriver</webdriver.chrome.driver>

        <!-- Our plugin key is usually composed of the project groupId and artifactId as defined above -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>

    <dependencies>
        <!-- Provided dependencies -->
<!--
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
        </dependency>
-->

        <!-- Common core dependencies -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <version>${atlassian.plugins.version}</version>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>biz.aQute.bnd</groupId>
                    <artifactId>biz.aQute.bndlib</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Common optional dependencies, remove if not required: -->
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <version>${atlassian.sal.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>${atlassian.rest.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <version>${atlassian.soy.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Spring Scanner -->
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
        </dependency>

        <!-- Dynamic Web Modules - optional -->
        <dependency>
            <groupId>com.adaptavist</groupId>
            <artifactId>dynamic-web-modules</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.pocketknife</groupId>
            <artifactId>atlassian-pocketknife-dynamic-modules</artifactId>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-client</artifactId>
        </dependency>

        <!-- Arquillian Dependencies -->
        <dependency>
            <groupId>org.jboss.arquillian.junit</groupId>
            <artifactId>arquillian-junit-container</artifactId>
        </dependency>
        <dependency>
            <groupId>com.adaptavist.arquillian.atlassian</groupId>
            <artifactId>remote-atlas-container</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.arquillian.graphene</groupId>
            <artifactId>graphene-webdriver</artifactId>
            <version>${arquillian.graphene.version}</version>
            <type>pom</type>
            <scope>test</scope>
        </dependency>
        <!-- javax.servlet.servlet-api is required as a 'test' scope dependency if not 'provided' as above -->

        <!-- SauceLabs test dependencies -->
        <dependency>
            <groupId>com.saucelabs</groupId>
            <artifactId>saucerest</artifactId>
            <version>${saucelabs.rest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.arquillian.extension</groupId>
            <artifactId>arquillian-drone-saucelabs-extension</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.jboss.arquillian.selenium</groupId>
                <artifactId>selenium-bom</artifactId>
                <version>${selenium.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.jboss.arquillian</groupId>
                <artifactId>arquillian-bom</artifactId>
                <version>${arquillian.junit.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.jboss.arquillian.extension</groupId>
                <artifactId>arquillian-drone-bom</artifactId>
                <version>${arquillian.drone.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
            <resource>
                <directory>src/main/frontend</directory>
            </resource>
            <resource>
                <directory>target/generated-resources</directory>
            </resource>
        </resources>

        <!-- test resource filtering evaluates ${...} expressions in arquillian.xml -->
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>

        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>

                <configuration>
                    <server>localhost</server>

                    <products>
                        <product>
                            <id>jira</id>
                            <instanceId>jira</instanceId>
                            <version>${jira.test.version}</version>
                            <dataVersion>${jira.test.version}</dataVersion>
                            <jvmDebugPort>5005</jvmDebugPort>

                            <systemPropertyVariables>
                                <jira.websudo.is.disabled>true</jira.websudo.is.disabled>
                                <atlassian.darkfeature.jira.onboarding.feature.disabled>true</atlassian.darkfeature.jira.onboarding.feature.disabled>
                            </systemPropertyVariables>
                        </product>

                        <product>
                            <id>confluence</id>
                            <instanceId>confluence</instanceId>
                            <version>${confluence.test.version}</version>
                            <dataVersion>${confluence.test.version}</dataVersion>
                            <jvmDebugPort>5006</jvmDebugPort>
                        </product>
                    </products>

                    <testGroups>
                        <testGroup>
                            <id>jira</id>
                            <productIds>
                                <productId>jira</productId>
                            </productIds>
                            <includes>
                                <include>it/**/rest/*Test.java</include>
                                <include>it/**/service/*Test.java</include>
                                <include>it/**/ui/*Suite.java</include>
                            </includes>
                        </testGroup>
                        <testGroup>
                            <id>confluence</id>
                            <productIds>
                                <productId>confluence</productId>
                            </productIds>
                            <includes>
                                <include>it/**/rest/*Test.java</include>
                                <include>it/**/service/*Test.java</include>
                                <include>it/**/ui/*Suite.java</include>
                            </includes>
                        </testGroup>
                        <testGroup>
                            <id>prepare-docker</id>
                            <productIds>
                                <productId>jira</productId>
                                <!-- <productId>confluence</productId> -->
                            </productIds>
                            <includes>
                                <include>**/PrepareDockerImage.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>

                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
                        <Spring-Context>*</Spring-Context>
                        <Export-Package>
                            com.adaptavist.reference.service;version="${project.version}"
                        </Export-Package>
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            com.atlassian.plugin.osgi.bridge.external,
                            org.springframework.core.*;version="${spring.version}",
                            *;resolution:=optional
                        </Import-Package>
                    </instructions>

                    <pluginArtifacts>
                        <!-- This plugin will configure debugging logging of com.adaptavist -->
                        <pluginArtifact>
                            <groupId>com.adaptavist</groupId>
                            <artifactId>logging-assistant</artifactId>
                            <version>1.0.0</version>
                        </pluginArtifact>

                        <!-- Install the Quick Reload plugin, to automatically re-install the plugin after rebuild -->
                        <pluginArtifact>
                            <groupId>com.atlassian.labs.plugins</groupId>
                            <artifactId>quickreload</artifactId>
                            <version>${quick.reload.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>

                    <extractDependencies>false</extractDependencies>
                    <extractTestDependencies>false</extractTestDependencies>
                    <compressResources>false</compressResources>
                    <enableFastdev>false</enableFastdev>
                    <useFastdevCli>false</useFastdevCli>
                    <skipAllPrompts>true</skipAllPrompts>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>

                <!-- NOTE: This configuration is only needed if you are using dynamic-web-modules -->
                <configuration>
                    <includeExclude>+com.adaptavist.*,+com.atlassian.pocketknife.*</includeExclude>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.adaptavist</groupId>
                            <artifactId>dynamic-web-modules</artifactId>
                        </dependency>
                        <dependency>
                            <groupId>com.atlassian.pocketknife</groupId>
                            <artifactId>atlassian-pocketknife-dynamic-modules</artifactId>
                        </dependency>
                    </scannedDependencies>
                </configuration>
            </plugin>

<!--
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>

                <executions>
                    &lt;!&ndash; NOTE: executions for node and npm download/install are inherited from the base pom &ndash;&gt;

                    <execution>
                        <id>grunt build</id>
                        <goals>
                            <goal>grunt</goal>
                        </goals>
                        <phase>generate-resources</phase>
                        <configuration>
                            <arguments>build</arguments>
                        </configuration>
                    </execution>

                    <execution>
                        <id>javascript unit tests</id>
                        <goals>
                            <goal>karma</goal>
                        </goals>
                        <phase>test</phase>
                        <configuration>
                            <karmaConfPath>karma-unit.conf.js &#45;&#45;single-run</karmaConfPath>
                        </configuration>
                    </execution>

                </executions>
            </plugin>
-->

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>1.9</version>
                <configuration>
                    <verbose>false</verbose>
                    <licenseName>apache_v2</licenseName>
                    <addJavaLicenseAfterPackage>false</addJavaLicenseAfterPackage>
                    <emptyLineAfterHeader>true</emptyLineAfterHeader>
                </configuration>
                <executions>
                    <execution>
                        <id>update-licenses</id>
                        <goals>
                            <goal>update-file-header</goal>
                        </goals>
                        <phase>process-sources</phase>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>confluence</id>

            <properties>
                <instanceId>confluence</instanceId>
                <testGroups>confluence</testGroups>
                <jacoco.it.group>it-confluence</jacoco.it.group>
            </properties>
        </profile>

        <profile>
            <id>jira</id>

            <properties>
                <instanceId>jira</instanceId>
                <testGroups>jira</testGroups>
                <jacoco.it.group>it-jira</jacoco.it.group>
            </properties>
        </profile>

        <profile>
            <id>prepare-docker</id>

            <properties>
                <testGroups>prepare-docker</testGroups>
                <maven.test.failure.ignore>true</maven.test.failure.ignore>
                <testFailureIgnore>true</testFailureIgnore>
            </properties>

            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <!-- skip unit tests, but not integration tests -->
                            <skipTests>true</skipTests>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>chrome</id>
            <properties>
                <browser>chrome</browser>
            </properties>
        </profile>

        <profile>
            <id>saucelabs</id>

            <properties>
                <browser>saucelabs</browser>

                <saucelabs.browser.name>chrome</saucelabs.browser.name>
                <saucelabs.browser.platform>Windows 10</saucelabs.browser.platform>
                <saucelabs.browser.version>beta</saucelabs.browser.version>
            </properties>
        </profile>

        <profile>
            <id>sauce-connect</id>

            <build>
                <plugins>
                    <plugin>
                        <groupId>com.saucelabs.maven.plugin</groupId>
                        <artifactId>sauce-connect-plugin</artifactId>
                        <version>${saucelabs.connect.version}</version>
                        <configuration>
                            <!--suppress MavenModelInspection -->
                            <sauceUsername>${saucelabs.username}</sauceUsername>
                            <!--suppress MavenModelInspection -->
                            <sauceAccessKey>${saucelabs.accesskey}</sauceAccessKey>
                            <!--suppress MavenModelInspection -->
                            <options>-i ${saucelabs.build.key}</options>
                        </configuration>
                        <executions>
                            <execution>
                                <id>start-sauceconnct</id>
                                <phase>pre-integration-test</phase>
                                <goals>
                                    <goal>start-sauceconnect</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>stop-sauceconnect</id>
                                <phase>post-integration-test</phase>
                                <goals>
                                    <goal>stop-sauceconnect</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>coverage-merge</id>

            <build>
                <plugins>
                    <plugin>
                        <groupId>org.jacoco</groupId>
                        <artifactId>jacoco-maven-plugin</artifactId>
                        <configuration>
                            <fileSets>
                                <fileSet>
                                    <directory>${project.build.directory}</directory>
                                    <includes>
                                        <include>jacoco-it-*.exec</include>
                                    </includes>
                                </fileSet>
                            </fileSets>
                            <destFile>${project.build.directory}/jacoco-it.exec</destFile>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>atlassian-public</id>
            <url>https://maven.atlassian.com/repository/public</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </repository>

        <repository>
            <id>adaptavist-external</id>
            <url>https://nexus.adaptavist.com/content/repositories/external</url>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>atlassian-public</id>
            <url>https://maven.atlassian.com/repository/public</url>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
            <snapshots>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
        </pluginRepository>

        <pluginRepository>
            <id>saucelabs-repository</id>
            <url>http://repository-saucelabs.forge.cloudbees.com/release</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>

    </pluginRepositories>

    <distributionManagement>
        <repository>
            <id>nexus</id>
            <name>External Releases</name>
            <url>https://nexus.adaptavist.com/content/repositories/external</url>
        </repository>
        <snapshotRepository>
            <id>nexus</id>
            <name>Internal Releases</name>
            <url>https://nexus.adaptavist.com/content/repositories/adaptavist-snapshots</url>
        </snapshotRepository>
    </distributionManagement>
</project>
