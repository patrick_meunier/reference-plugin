# Adaptavist Reference Plugin

This is an example plugin for use as a reference for training and
experimental purposes.

# Building and development

    $ ./build.sh

You can start up either JIRA or Confluence:

    $ ./jira.sh

    $ ./confluence.sh

After making changes to the plugin you can rebuild with:

    $ ./rebuild.sh

The plugin will be automatically re-installed via Quick Reload.

# Importing Into IntelliJ IDEA

There are 2 ways to import the project into IDEA:

## CommandLine

* Make sure IDEA is running
* In the project folder type: `idea .`

This will import/open the project in IDEA without stepping through the import configuration pages. (See next)

## GUI

* Click on 'Import Project'
* Select the project root folder
* Select 'Import project from external model'
    * Select 'Maven'
* Click 'Next' and go through the remaining import pages
    * If you have more than 1 JDK availble, be sure to select version 1.8

# Tests

If you want to run the browser tests on Chrome you'll need to install
chromedriver, on Mac OSX:

    $ brew install chromedriver
    $ which chromedriver
    
Copy the path into <webdriver.chrome.driver> in the pom.xml properties.

# SauceLabs

The tests are currently running on a internal Bamboo plan but using an
open source Sauce Labs accounts, the results of which can be seen here:

https://saucelabs.com/open_sauce/user/avst-ref-plugin

(We still need to report the success/failure status)


